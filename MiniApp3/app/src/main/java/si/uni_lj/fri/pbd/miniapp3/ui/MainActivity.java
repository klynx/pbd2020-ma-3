package si.uni_lj.fri.pbd.miniapp3.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;

import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.ui.favorites.FavoritesFragment;
import si.uni_lj.fri.pbd.miniapp3.ui.search.SearchFragment;

public class MainActivity extends AppCompatActivity {

    ViewPager2 mViewPager2;
    SectionsPagerAdapter mAdapter;
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set toolbar tabs
        setToolbar();
    }

    private void setToolbar() {
        // get adapter
        mAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), getLifecycle());

        // get viewPager
        mViewPager2 = findViewById(R.id.viewPager);
        mViewPager2.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        mViewPager2.setAdapter(mAdapter);

        // set tab selection to sync with ui
        TabLayout tl = findViewById(R.id.tab_layout);
        new TabLayoutMediator(tl, mViewPager2, (tab, position) -> {
            switch (position) {
                case 0:
                    tab.setText(R.string.search_tab_title);
                    break;
                case 1:
                    tab.setText(R.string.favorites_tab_title);
                    break;
                default:
                    tab.setText("omegaLuL what");
            }
        }).attach();
        Log.d(TAG, "Toolbar set");
    }

    /**
     * Tab adapter class. Sets the Tabs for SearchFragment and favoritesFragment
     */
    class SectionsPagerAdapter extends FragmentStateAdapter {

        SectionsPagerAdapter(@NonNull FragmentManager fragmentmanager, @NonNull Lifecycle lifeCycle) {
            super(fragmentmanager, lifeCycle);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            switch (position) {
                case 0:
                    return new SearchFragment();
                case 1:
                    return new FavoritesFragment();
            }
            // tough shit
            return null;
        }

        @Override
        public int getItemCount() {
            return 2;
        }
    }
}
