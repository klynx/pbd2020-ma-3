package si.uni_lj.fri.pbd.miniapp3.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeDetailsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIdDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;

public class DetailsActivity extends AppCompatActivity {
    private static final String TAG = DetailsActivity.class.getSimpleName();
    private String recipeId;
    private RestAPI mRestClient;
    private RecipeDetailsDTO recipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Bundle extras = getIntent().getExtras();
        String source = null;
        if (extras != null) {
            recipeId = extras.getString("id");
            Log.d(TAG, recipeId);
            source = extras.getString("source");
        }

        if (source == null) {
            Log.d(TAG, "could not get source");
            return;
        }

        // if intent launched from searchFragment get data from API
        if (source.equals("search")) {
            Log.d(TAG, "activity launched from SearchFragment. Getting data from API");
            mRestClient = ServiceGenerator.createService(RestAPI.class);
            getDataFromAPI();
        }

        // if intent launched from FavoritesFragment get data from database
        if (source.equals("favorites")) {
            Log.d(TAG, "activity launched from FavoritesFragment. Getting data from database");
            // TODO: cry
            // ^ actually that's already done
        }
    }


    private void getDataFromAPI() {
        mRestClient.getRecipesById(recipeId)
                .enqueue(new Callback<RecipesByIdDTO>() {
                    @Override
                    public void onResponse(Call<RecipesByIdDTO> call, Response<RecipesByIdDTO> response) {
                        if (!response.isSuccessful()) {
                            Log.e(TAG, "response was not successful");
                        }

                        RecipeDetailsDTO result = response.body().getMeals().get(0);
                        // store result if successful
                        if (result != null) {

                            recipe = result;
                            // display the data
                            displayData();
                        }

                        Log.d(TAG, String.valueOf("is result null " + result == null));

                        Log.d(TAG, result.toString());
                    }

                    @Override
                    public void onFailure(Call<RecipesByIdDTO> call, Throwable t) {
                    }
                });
    }

    // display data, after successful api call
    private void displayData() {
        Log.d(TAG, "displayData");
        // display image
        ImageView image = findViewById(R.id.recipe_image);
        new DownloadImageTask(image).execute(recipe.getStrMealThumb());

        // display name
        TextView name = findViewById(R.id.strMeal);
        name.setText(recipe.getStrMeal());

        // display all ingredients
        TextView ing1 = findViewById(R.id.ing1);
        if(recipe.getStrIngredient1() != null || recipe.getStrMeasure1() != null) {
            Log.d(TAG, "I have ingredient1");
            ing1.setText(recipe.getStrIngredient1() + ": " + recipe.getStrMeasure1());
        } else {
            ing1.setVisibility(View.GONE);
        }

        TextView ing2 = findViewById(R.id.ing2);
        if(recipe.getStrIngredient2() != null || recipe.getStrMeasure2() != null) {
            Log.d(TAG, "I have ingredient2");
            ing2.setText(recipe.getStrIngredient2() + ": " + recipe.getStrMeasure2());
        } else {
            ing2.setVisibility(View.GONE);
        }

        TextView ing3 = findViewById(R.id.ing3);
        if(recipe.getStrIngredient3() != null || recipe.getStrMeasure3() != null) {
            Log.d(TAG, "I have ingredient3");
            ing3.setText(recipe.getStrIngredient3() + ": " + recipe.getStrMeasure3());
        } else {
            ing3.setVisibility(View.GONE);
        }

        TextView ing4 = findViewById(R.id.ing4);
        if(recipe.getStrIngredient4() != null || recipe.getStrMeasure4() != null) {
            Log.d(TAG, "I have ingredient4");
            ing4.setText(recipe.getStrIngredient4() + ": " + recipe.getStrMeasure4());
        } else {
           ing4.setVisibility(View.GONE);
        }

        TextView ing5 = findViewById(R.id.ing5);
        if(recipe.getStrIngredient5() != null || recipe.getStrMeasure5() != null) {
            Log.d(TAG, "I have ingredient5");
            ing5.setText(recipe.getStrIngredient5() + " " + recipe.getStrMeasure5());
        } else {
            ing5.setVisibility(View.GONE);
        }

        TextView ing6 = findViewById(R.id.ing6);
        if(recipe.getStrIngredient6() != null || recipe.getStrMeasure6() != null) {
            Log.d(TAG, "I have ingredient6");
            ing6.setText(recipe.getStrIngredient6() + " " + recipe.getStrMeasure6());
        } else {
            ing6.setVisibility(View.GONE);
        }
        TextView ing7 = findViewById(R.id.ing7);
        if(recipe.getStrIngredient7() != null || recipe.getStrMeasure7() != null) {
            Log.d(TAG, "I have ingredient7");
            ing7.setText(recipe.getStrIngredient7() + " " + recipe.getStrMeasure7());
        } else {
            ing7.setVisibility(View.GONE);
        }
        TextView ing8 = findViewById(R.id.ing8);
        if(recipe.getStrIngredient8() != null || recipe.getStrMeasure8() != null) {
            Log.d(TAG, "I have ingredient8");
            ing8.setText(recipe.getStrIngredient8() + " " + recipe.getStrMeasure8());
        } else {
            ing8.setVisibility(View.GONE);
        }
        TextView ing9 = findViewById(R.id.ing9);
        if(recipe.getStrIngredient9() != null || recipe.getStrMeasure9() != null) {
            Log.d(TAG, "I have ingredient9");
            ing9.setText(recipe.getStrIngredient9() + " " + recipe.getStrMeasure9());
        } else {
            ing9.setVisibility(View.GONE);
        }
        TextView ing10 = findViewById(R.id.ing10);
        if(recipe.getStrIngredient10() != null || recipe.getStrMeasure10() != null) {
            Log.d(TAG, "I have ingredient10");
            ing10.setText(recipe.getStrIngredient10() + " " + recipe.getStrMeasure10());
        } else {
            ing10.setVisibility(View.GONE);
        }
        TextView ing11 = findViewById(R.id.ing11);
        if(recipe.getStrIngredient11() != null || recipe.getStrMeasure11() != null) {
            Log.d(TAG, "I have ingredient11");
            ing11.setText(recipe.getStrIngredient11() + " " + recipe.getStrMeasure11());
        } else {
            ing11.setVisibility(View.GONE);
        }
        TextView ing12 = findViewById(R.id.ing12);
        if(recipe.getStrIngredient12() != null || recipe.getStrMeasure12() != null) {
            Log.d(TAG, "I have ingredient12");
            ing12.setText(recipe.getStrIngredient12() + " " + recipe.getStrMeasure12());
        } else {
            ing12.setVisibility(View.GONE);
        }
        TextView ing13 = findViewById(R.id.ing13);
        if(recipe.getStrIngredient13() != null || recipe.getStrMeasure13() != null) {
            Log.d(TAG, "I have ingredient13");
            ing13.setText(recipe.getStrIngredient13() + " " + recipe.getStrMeasure13());
        } else {
            ing13.setVisibility(View.GONE);
        }
        TextView ing14 = findViewById(R.id.ing14);
        if(recipe.getStrIngredient14() != null || recipe.getStrMeasure14() != null) {
            Log.d(TAG, "I have ingredient14");
            ing14.setText(recipe.getStrIngredient14() + " " + recipe.getStrMeasure14());
        } else {
            ing14.setVisibility(View.GONE);
        }
        TextView ing15 = findViewById(R.id.ing15);
        if(recipe.getStrIngredient15() != null || recipe.getStrMeasure15() != null) {
            Log.d(TAG, "I have ingredient15");
            ing15.setText(recipe.getStrIngredient15() + " " + recipe.getStrMeasure15());
        } else {
            ing15.setVisibility(View.GONE);
        }
        TextView ing16 = findViewById(R.id.ing16);
        if(recipe.getStrIngredient16() != null || recipe.getStrMeasure16() != null) {
            Log.d(TAG, "I have ingredient16");
            ing16.setText(recipe.getStrIngredient16() + " " + recipe.getStrMeasure16());
        } else {
            ing16.setVisibility(View.GONE);
        }
        TextView ing17 = findViewById(R.id.ing17);
        if(recipe.getStrIngredient17() != null || recipe.getStrMeasure17() != null) {
            Log.d(TAG, "I have ingredient17");
            ing17.setText(recipe.getStrIngredient17() + " " + recipe.getStrMeasure17());
        } else {
            ing17.setVisibility(View.GONE);
        }
        TextView ing18 = findViewById(R.id.ing18);
        if(recipe.getStrIngredient18() != null || recipe.getStrMeasure18() != null) {
            Log.d(TAG, "I have ingredient18");
            ing18.setText(recipe.getStrIngredient18() + " " + recipe.getStrMeasure18());
        } else {
            ing18.setVisibility(View.GONE);
        }
        TextView ing19 = findViewById(R.id.ing19);
        if(recipe.getStrIngredient19() != null || recipe.getStrMeasure19() != null) {
            Log.d(TAG, "I have ingredient19");
            ing19.setText(recipe.getStrIngredient19() + " " + recipe.getStrMeasure19());
        } else {
            ing19.setVisibility(View.GONE);
        }
        TextView ing20 = findViewById(R.id.ing20);
        if(recipe.getStrIngredient20() != null || recipe.getStrMeasure20() != null) {
            Log.d(TAG, "I have ingredient20");
            ing20.setText(recipe.getStrIngredient20() + " " + recipe.getStrMeasure20());
        } else {
            ing20.setVisibility(View.GONE);
        }

        // display intstruction
        TextView instructions = findViewById(R.id.strInstructions);
        instructions.setText(recipe.getStrInstructions());

    }

    // sets image of recipe
    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        ImageView imgV;

        DownloadImageTask(final ImageView imgV) {
            this.imgV = imgV;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            final String urlString = urls[0];

            URL url = null;
            try {
                url = new URL(urlString);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Bitmap bmp = null;
            try {
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bmp;
        }

        protected void onPostExecute(Bitmap result) {
            imgV.setImageBitmap(result);
        }
    }
}
