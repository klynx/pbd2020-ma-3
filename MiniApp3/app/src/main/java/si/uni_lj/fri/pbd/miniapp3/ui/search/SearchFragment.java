package si.uni_lj.fri.pbd.miniapp3.ui.search;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeSummaryDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;
import si.uni_lj.fri.pbd.miniapp3.ui.DetailsActivity;


public class SearchFragment extends Fragment implements AdapterView.OnItemSelectedListener{
    private static final String TAG = SearchFragment.class.getSimpleName();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RestAPI mRestClient;
    private SpinnerAdapter sa;
    RecyclerView recyclerView;

    // TODO: Rename and change types of parameters

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        mRestClient = ServiceGenerator.createService(RestAPI.class);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");

        //create spinner Adapter
        Spinner spinner = getView().findViewById(R.id.spinner);

        // get all ingredients
        mRestClient.getAllIngredients()
                .enqueue(
                        new Callback<IngredientsDTO>() {
                            @Override
                            public void onResponse(Call<IngredientsDTO> call, Response<IngredientsDTO> response) {
                                if (!response.isSuccessful()) {
                                    Log.d(TAG, "response is not successful");
                                    return;
                                }
                                sa = new SpinnerAdapter(getContext(), response.body());
                                spinner.setAdapter(sa);
                            }

                            @Override
                            public void onFailure(Call<IngredientsDTO> call, Throwable t) {
                                Log.d(TAG, "call failed");
                                Log.d(TAG, t.toString());
                            }
                        }
                );
        // set spinner listener
        spinner.setOnItemSelectedListener(this);

        // set recyclerview
        recyclerView = getView().findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }


    // Item selected callback
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "selected item " + position);
        IngredientDTO item = (IngredientDTO) sa.getItem(position);
        Log.d(TAG, item.getStrIngredient());

        //get all recipes for ingredient
        mRestClient.getRecipesForIngredient(item.getStrIngredient())
                .enqueue(new Callback<RecipesByIngredientDTO>() {
                    @Override
                    public void onResponse(Call<RecipesByIngredientDTO> call, Response<RecipesByIngredientDTO> response) {
                        if (!response.isSuccessful()) {
                            Log.d(TAG, "getting recipes was not successful");
                            return;
                        }
                        List<RecipeSummaryDTO> recipes = response.body().getMeals();

                        // set adapter for recipe list
                        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(), recipes);
                        recyclerView.setAdapter(adapter);

                    }

                    @Override
                    public void onFailure(Call<RecipesByIngredientDTO> call, Throwable t) {
                        Log.e(TAG, "error getting recipes");
                        Log.e(TAG, t.toString());
                    }
                });
        Log.d(TAG, "onSelected ends");
    }

    // if nothing selected callback
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Log.d(TAG, "selected nothing");
    }

   class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
        private Context context;
        private List<RecipeSummaryDTO> recipes;
        private LayoutInflater mInflater;

        RecyclerViewAdapter(Context context, List<RecipeSummaryDTO> recipes) {
            this.context = context;
            this.recipes = recipes;
            this.mInflater = LayoutInflater.from(context);
        }

       @NonNull
       @Override
       public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.layout_grid_item, parent, false);
           return new ViewHolder(view);
       }

       @Override
       public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder holder, int position) {
            RecipeSummaryDTO recipe = recipes.get(position);
            holder.name.setText(recipe.getStrMeal());
            holder.repiceId = recipe.getIdMeal();
            new DownloadImageTask(holder.image).execute(recipe.getStrMealThumb());
       }

       @Override
       public int getItemCount() {
           return recipes == null ? 0 : recipes.size();
       }

       public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

           TextView name;
           ImageView image;
           String repiceId;

           public ViewHolder(@NonNull View itemView) {
               super(itemView);
               name = itemView.findViewById(R.id.text_view_content);
               image = itemView.findViewById(R.id.image_view);
               itemView.setOnClickListener(this);
           }

           @Override
           public void onClick(View v) {
               Log.d(TAG, "onItemClick");
               Intent details = new Intent(getContext(), DetailsActivity.class);
               details.putExtra("source", "search");
               details.putExtra("id", repiceId);
               startActivity(details);
           }
       }
   };

    // spinner adapter, controls the ingredients in the spinner
    class SpinnerAdapter extends BaseAdapter {
        private Context context;
        private IngredientsDTO items;

        SpinnerAdapter(Context context, IngredientsDTO items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.getIngredients().size();
        }

        @Override
        public Object getItem(int position) {
            return items.getIngredients().get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.spinner_item, parent, false);
            }

            IngredientDTO item = (IngredientDTO) getItem(position);
            TextView name = (TextView) convertView.findViewById(R.id.text_view_spinner_item);
            name.setText(item.getStrIngredient());
            return convertView;
        }
    }

    // sets images for each recipe
    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        ImageView imgV;

        DownloadImageTask(final ImageView imgV) {
            this.imgV = imgV;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            final String urlString = urls[0];

            URL url = null;
            try {
                url = new URL(urlString);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Bitmap bmp = null;
            try {
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bmp;
        }

        protected void onPostExecute(Bitmap result) {
            imgV.setImageBitmap(result);
        }
    }

}
