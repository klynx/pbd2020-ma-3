package si.uni_lj.fri.pbd.miniapp3.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import si.uni_lj.fri.pbd.miniapp3.R;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = SplashActivity.class.getSimpleName();
    private static final int splashDelay  = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    public void onStart(){
        super.onStart();

        Log.d(TAG, "onStart");

        // start main activity after delay
        new Handler().postDelayed(() -> {
                Log.d(TAG, "starting main activity");
                Intent startMain = new Intent(this, MainActivity.class);
                startActivity(startMain);
                finish();
        }, splashDelay);
    }

}
