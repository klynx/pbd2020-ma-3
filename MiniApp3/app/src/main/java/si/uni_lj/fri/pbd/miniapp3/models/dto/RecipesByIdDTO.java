package si.uni_lj.fri.pbd.miniapp3.models.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;

public class RecipesByIdDTO {
    public List<RecipeDetailsDTO> getMeals() {
        return meals;
    }

    public void setMeals(List<RecipeDetailsDTO> meals) {
        this.meals = meals;
    }

    @Expose
    List<RecipeDetailsDTO> meals;
}
